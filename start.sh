#!/bin/bash

cd main
pm2 start ./apps.json
cd ..

cd web
pm2 start npm --name "web" -- start
cd ..
