module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/button");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/input");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/form");

/***/ }),
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(8);


/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "antd/lib/locale-provider"
var locale_provider_ = __webpack_require__(9);
var locale_provider__default = /*#__PURE__*/__webpack_require__.n(locale_provider_);

// EXTERNAL MODULE: external "antd/lib/modal"
var modal_ = __webpack_require__(10);
var modal__default = /*#__PURE__*/__webpack_require__.n(modal_);

// EXTERNAL MODULE: external "antd/lib/table"
var table_ = __webpack_require__(11);
var table__default = /*#__PURE__*/__webpack_require__.n(table_);

// EXTERNAL MODULE: external "antd/lib/alert"
var alert_ = __webpack_require__(12);
var alert__default = /*#__PURE__*/__webpack_require__.n(alert_);

// EXTERNAL MODULE: external "antd/lib/col"
var col_ = __webpack_require__(13);
var col__default = /*#__PURE__*/__webpack_require__.n(col_);

// EXTERNAL MODULE: external "antd/lib/button"
var button_ = __webpack_require__(1);
var button__default = /*#__PURE__*/__webpack_require__.n(button_);

// EXTERNAL MODULE: external "antd/lib/row"
var row_ = __webpack_require__(14);
var row__default = /*#__PURE__*/__webpack_require__.n(row_);

// EXTERNAL MODULE: external "antd/lib/message"
var message_ = __webpack_require__(15);
var message__default = /*#__PURE__*/__webpack_require__.n(message_);

// EXTERNAL MODULE: external "antd/lib/collapse"
var collapse_ = __webpack_require__(16);
var collapse__default = /*#__PURE__*/__webpack_require__.n(collapse_);

// EXTERNAL MODULE: external "antd/lib/layout"
var layout_ = __webpack_require__(17);
var layout__default = /*#__PURE__*/__webpack_require__.n(layout_);

// EXTERNAL MODULE: external "react-shapes"
var external__react_shapes_ = __webpack_require__(18);
var external__react_shapes__default = /*#__PURE__*/__webpack_require__.n(external__react_shapes_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "antd/lib/input"
var input_ = __webpack_require__(2);
var input__default = /*#__PURE__*/__webpack_require__.n(input_);

// EXTERNAL MODULE: external "antd/lib/form"
var form_ = __webpack_require__(3);
var form__default = /*#__PURE__*/__webpack_require__.n(form_);

// CONCATENATED MODULE: ./components/AccessCodeSettings.js




var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var FormItem = form__default.a.Item;

var AccessCodeSettings_Settings = function (_Component) {
  _inherits(Settings, _Component);

  function Settings() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Settings);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Settings.__proto__ || Object.getPrototypeOf(Settings)).call.apply(_ref, [this].concat(args))), _this), _this.handleSubmit = function (e) {
      e.preventDefault();
      _this.props.form.validateFieldsAndScroll(function (err, values) {
        if (!err) {
          _this.props.handleSubmit(values);
        }
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Settings, [{
    key: 'render',
    value: function render() {
      var getFieldDecorator = this.props.form.getFieldDecorator;
      var accessCode = this.props.accessCode;


      return external__react__default.a.createElement(
        form__default.a,
        { layout: 'horizontal', onSubmit: this.handleSubmit },
        external__react__default.a.createElement(
          FormItem,
          {
            label: '\u6CE8\u518C\u7801',
            labelCol: { span: 8 },
            wrapperCol: { span: 8 }
          },
          getFieldDecorator('accessCode', {
            initialValue: accessCode,
            rules: [{
              required: true,
              message: '请输入注册码'
            }]
          })(external__react__default.a.createElement(input__default.a, null))
        ),
        external__react__default.a.createElement(
          FormItem,
          { wrapperCol: { span: 8, offset: 8 } },
          external__react__default.a.createElement(
            button__default.a,
            { type: 'primary', htmlType: 'submit' },
            '\u8BBE\u7F6E'
          )
        )
      );
    }
  }]);

  return Settings;
}(external__react_["Component"]);

/* harmony default export */ var AccessCodeSettings = (form__default.a.create()(AccessCodeSettings_Settings));
// CONCATENATED MODULE: ./components/ServerSettings.js




var ServerSettings__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function ServerSettings__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function ServerSettings__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function ServerSettings__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var ServerSettings_FormItem = form__default.a.Item;

var ServerSettings_Settings = function (_Component) {
  ServerSettings__inherits(Settings, _Component);

  function Settings() {
    var _ref;

    var _temp, _this, _ret;

    ServerSettings__classCallCheck(this, Settings);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = ServerSettings__possibleConstructorReturn(this, (_ref = Settings.__proto__ || Object.getPrototypeOf(Settings)).call.apply(_ref, [this].concat(args))), _this), _this.handleSubmit = function (e) {
      e.preventDefault();
      _this.props.form.validateFieldsAndScroll(function (err, values) {
        if (!err) {
          _this.props.handleSubmit(values);
        }
      });
    }, _temp), ServerSettings__possibleConstructorReturn(_this, _ret);
  }

  ServerSettings__createClass(Settings, [{
    key: 'render',
    value: function render() {
      var getFieldDecorator = this.props.form.getFieldDecorator;
      var server = this.props.server;


      return external__react__default.a.createElement(
        'div',
        null,
        external__react__default.a.createElement(
          'h4',
          null,
          '\u670D\u52A1\u5668'
        ),
        external__react__default.a.createElement(
          form__default.a,
          { layout: 'horizontal', onSubmit: this.handleSubmit },
          external__react__default.a.createElement(
            ServerSettings_FormItem,
            {
              label: '\u670D\u52A1\u5668\u5730\u5740',
              labelCol: { span: 8 },
              wrapperCol: { span: 8 }
            },
            getFieldDecorator('server', {
              initialValue: server,
              rules: [{
                required: true,
                message: '请输入服务器地址'
              }]
            })(external__react__default.a.createElement(input__default.a, null))
          ),
          external__react__default.a.createElement(
            ServerSettings_FormItem,
            { wrapperCol: { span: 8, offset: 8 } },
            external__react__default.a.createElement(
              button__default.a,
              { type: 'primary', htmlType: 'submit' },
              '\u8BBE\u7F6E'
            )
          )
        )
      );
    }
  }]);

  return Settings;
}(external__react_["Component"]);

/* harmony default export */ var ServerSettings = (form__default.a.create()(ServerSettings_Settings));
// EXTERNAL MODULE: external "antd/lib/radio"
var radio_ = __webpack_require__(19);
var radio__default = /*#__PURE__*/__webpack_require__.n(radio_);

// CONCATENATED MODULE: ./components/GatewaySettings.js





var GatewaySettings__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function GatewaySettings__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function GatewaySettings__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function GatewaySettings__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var GatewaySettings_FormItem = form__default.a.Item;
var RadioGroup = radio__default.a.Group;

var GatewaySettings_Settings = function (_Component) {
  GatewaySettings__inherits(Settings, _Component);

  function Settings(props) {
    GatewaySettings__classCallCheck(this, Settings);

    var _this = GatewaySettings__possibleConstructorReturn(this, (Settings.__proto__ || Object.getPrototypeOf(Settings)).call(this, props));

    _this.handleSubmit = function (e) {
      e.preventDefault();
      _this.props.form.validateFieldsAndScroll(function (err, values) {
        if (!err) {
          _this.props.handleSubmit(values);
        }
      });
    };

    _this.handleDhcpChange = function () {
      setTimeout(function () {
        var dhcp = _this.props.form.getFieldValue('dhcp');

        _this.setState({ dhcp: dhcp });
      }, 0);
    };

    _this.validate = function (rule, value, callback) {
      if (value) {
        var re = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

        if (!re.test(value)) {
          callback('IP 不合法');
        } else {
          callback();
        }
      } else {
        callback();
      }
    };

    _this.state = {
      dhcp: _this.props.dhcp
    };
    return _this;
  }

  GatewaySettings__createClass(Settings, [{
    key: 'render',
    value: function render() {
      var getFieldDecorator = this.props.form.getFieldDecorator;
      var _props = this.props,
          dhcp = _props.dhcp,
          ip = _props.ip,
          mask = _props.mask,
          gateway = _props.gateway,
          dns = _props.dns;

      var disabled = this.state.dhcp == 'on';

      return external__react__default.a.createElement(
        'div',
        null,
        external__react__default.a.createElement(
          'h4',
          null,
          'Modbus Gateway \u7F51\u53E3'
        ),
        external__react__default.a.createElement(
          form__default.a,
          { layout: 'horizontal', onSubmit: this.handleSubmit },
          external__react__default.a.createElement(
            GatewaySettings_FormItem,
            {
              label: 'DHCP',
              labelCol: { span: 8 },
              wrapperCol: { span: 8 }
            },
            getFieldDecorator('dhcp', {
              initialValue: dhcp,
              rules: [{
                required: true,
                message: '请输入 IP'
              }],
              onChange: this.handleDhcpChange
            })(external__react__default.a.createElement(
              RadioGroup,
              null,
              external__react__default.a.createElement(
                radio__default.a,
                { value: 'on' },
                '\u5F00\u542F'
              ),
              external__react__default.a.createElement(
                radio__default.a,
                { value: 'off' },
                '\u5173\u95ED'
              )
            ))
          ),
          external__react__default.a.createElement(
            GatewaySettings_FormItem,
            { label: 'IP', labelCol: { span: 8 }, wrapperCol: { span: 8 } },
            getFieldDecorator('ip', {
              initialValue: ip,
              rules: [{
                required: true,
                message: '请输入 IP'
              }, {
                validator: this.validate
              }]
            })(external__react__default.a.createElement(input__default.a, { disabled: disabled }))
          ),
          external__react__default.a.createElement(
            GatewaySettings_FormItem,
            {
              label: '\u5B50\u7F51\u63A9\u7801',
              labelCol: { span: 8 },
              wrapperCol: { span: 8 }
            },
            getFieldDecorator('mask', {
              initialValue: mask,
              rules: [{
                required: true,
                message: '请输入子网掩码'
              }, {
                validator: this.validate
              }]
            })(external__react__default.a.createElement(input__default.a, { disabled: disabled }))
          ),
          external__react__default.a.createElement(
            GatewaySettings_FormItem,
            {
              label: '\u7F51\u5173',
              labelCol: { span: 8 },
              wrapperCol: { span: 8 }
            },
            getFieldDecorator('gateway', {
              initialValue: gateway,
              rules: [{
                required: true,
                message: '请输入网关'
              }, {
                validator: this.validate
              }]
            })(external__react__default.a.createElement(input__default.a, { disabled: disabled }))
          ),
          external__react__default.a.createElement(
            GatewaySettings_FormItem,
            { label: 'DNS', labelCol: { span: 8 }, wrapperCol: { span: 8 } },
            getFieldDecorator('dns', {
              initialValue: dns,
              rules: [{
                required: true,
                message: '请输入 DNS'
              }, {
                validator: this.validate
              }]
            })(external__react__default.a.createElement(input__default.a, { disabled: disabled }))
          ),
          external__react__default.a.createElement(
            GatewaySettings_FormItem,
            { wrapperCol: { span: 8, offset: 8 } },
            external__react__default.a.createElement(
              button__default.a,
              { type: 'primary', htmlType: 'submit' },
              '\u8BBE\u7F6E'
            )
          )
        )
      );
    }
  }]);

  return Settings;
}(external__react_["Component"]);

/* harmony default export */ var GatewaySettings = (form__default.a.create()(GatewaySettings_Settings));
// EXTERNAL MODULE: external "axios"
var external__axios_ = __webpack_require__(20);
var external__axios__default = /*#__PURE__*/__webpack_require__.n(external__axios_);

// EXTERNAL MODULE: external "antd/lib/locale-provider/zh_CN"
var zh_CN_ = __webpack_require__(21);
var zh_CN__default = /*#__PURE__*/__webpack_require__.n(zh_CN_);

// EXTERNAL MODULE: external "moment/locale/zh-cn"
var zh_cn_ = __webpack_require__(22);
var zh_cn__default = /*#__PURE__*/__webpack_require__.n(zh_cn_);

// EXTERNAL MODULE: ./ant-theme-vars.less
var ant_theme_vars = __webpack_require__(23);
var ant_theme_vars_default = /*#__PURE__*/__webpack_require__.n(ant_theme_vars);

// CONCATENATED MODULE: ./pages/index.js











var pages__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function pages__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function pages__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function pages__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Header = layout__default.a.Header,
    Content = layout__default.a.Content,
    Footer = layout__default.a.Footer;
var Panel = collapse__default.a.Panel;












var pages_Home = function (_Component) {
  pages__inherits(Home, _Component);

  function Home() {
    var _ref;

    var _temp, _this, _ret;

    pages__classCallCheck(this, Home);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = pages__possibleConstructorReturn(this, (_ref = Home.__proto__ || Object.getPrototypeOf(Home)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      visible: false,
      accessCode: '',
      server: '',

      serverState: 'online',
      localConfig: {},
      serverConfig: {}
    }, _this.showModal = function () {
      _this.setState({
        visible: true
      });
    }, _this.handleCancel = function (e) {
      _this.setState({
        visible: false
      });
    }, _this.handleSubmit = function (values) {
      _this.configServer.post('/localConfig', values).then(function (data) {
        message__default.a.success('设置成功');
      });
    }, _temp), pages__possibleConstructorReturn(_this, _ret);
  }

  pages__createClass(Home, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      this.configServer = external__axios__default.a.create({
        baseURL: '//' + window.location.hostname + ':9902/'
      });

      this.configServer.get('/config').then(function (response) {
        _this2.setState(response.data);
      }).catch(function (err) {
        message__default.a.error('获取配置失败');
      });

      this.timer = setInterval(function () {
        _this2.configServer.get('/config').then(function (response) {
          _this2.setState(response.data);
        });
      }, 5000);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      clearInterval(this.timer);
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          visible = _state.visible,
          serverState = _state.serverState,
          localConfig = _state.localConfig,
          serverConfig = _state.serverConfig;
      var accessCode = localConfig.accessCode,
          server = localConfig.server,
          dhcp = localConfig.dhcp,
          ip = localConfig.ip,
          mask = localConfig.mask,
          gateway = localConfig.gateway,
          dns = localConfig.dns;
      var code = serverConfig.code,
          note = serverConfig.note,
          state = serverConfig.state,
          _serverConfig$modbusT = serverConfig.modbusTcpDevices,
          modbusTcpDevices = _serverConfig$modbusT === undefined ? [] : _serverConfig$modbusT,
          _serverConfig$modbusR = serverConfig.modbusRtuDevices,
          modbusRtuDevices = _serverConfig$modbusR === undefined ? [] : _serverConfig$modbusR;


      var devices = [];

      if (serverState == 'online') {
        modbusTcpDevices.forEach(function (device, i) {
          devices.push({
            key: 'tcp-' + i,
            code: device.code,
            note: device.note,
            typeName: device.typeName,
            address: device.ip + ':' + device.port,
            state: device.state,
            protocol: 'Modbus TCP'
          });
        });

        modbusRtuDevices.forEach(function (device, i) {
          devices.push({
            key: 'rtu-' + i,
            code: device.code,
            note: device.note,
            typeName: device.typeName,
            address: '' + device.slaveId,
            state: device.state,
            protocol: 'Modbus RTU'
          });
        });
      }

      var columns = [{
        title: '设备编号',
        dataIndex: 'code',
        key: 'code'
      }, {
        title: '备注',
        dataIndex: 'note',
        key: 'note'
      }, {
        title: '设备类型',
        dataIndex: 'typeName',
        key: 'typeName'
      }, {
        title: '通信协议',
        dataIndex: 'protocol',
        key: 'protocol'
      }, {
        title: '从机地址',
        dataIndex: 'address',
        key: 'address'
        // {
        //   title: '连接状态',
        //   dataIndex: 'state',
        //   key: 'state',
        //   render: text => (
        //     <Circle
        //       r={10}
        //       fill={{
        //         color: text == 'offline' ? '#f04134' : '#00bf60'
        //       }}
        //       stroke={{ color: 'white' }}
        //       strokeWidth={3}
        //     />
        //   )
        // }
      }];

      return external__react__default.a.createElement(
        locale_provider__default.a,
        { locale: zh_CN__default.a },
        external__react__default.a.createElement(
          row__default.a,
          { type: 'flex', justify: 'center' },
          external__react__default.a.createElement(
            col__default.a,
            null,
            external__react__default.a.createElement(
              layout__default.a,
              null,
              external__react__default.a.createElement(
                Header,
                null,
                external__react__default.a.createElement(
                  row__default.a,
                  {
                    type: 'flex',
                    justify: 'space-between',
                    style: { minWidth: 800 }
                  },
                  external__react__default.a.createElement(
                    row__default.a,
                    { type: 'flex' },
                    external__react__default.a.createElement(
                      'h2',
                      {
                        style: {
                          color: 'white',
                          padding: '0 15px',
                          background: 'rgba(255,255,255,.2)'
                        }
                      },
                      'Modbus Gateway'
                    ),
                    serverState == 'online' && code ? external__react__default.a.createElement(
                      'div',
                      {
                        style: {
                          marginLeft: 30,
                          color: 'white'
                        }
                      },
                      external__react__default.a.createElement(
                        'div',
                        {
                          style: {
                            marginTop: -10,
                            height: 23,
                            display: 'flex'
                          }
                        },
                        external__react__default.a.createElement(
                          'span',
                          { style: { fontSize: 18 } },
                          code
                        )
                      ),
                      external__react__default.a.createElement(
                        'div',
                        null,
                        external__react__default.a.createElement(
                          'span',
                          { style: { color: '#ccc' } },
                          note
                        )
                      )
                    ) : null
                  ),
                  external__react__default.a.createElement(
                    col__default.a,
                    null,
                    external__react__default.a.createElement(button__default.a, {
                      shape: 'circle',
                      icon: 'setting',
                      onClick: this.showModal
                    })
                  )
                )
              ),
              external__react__default.a.createElement(
                Content,
                { style: { padding: '15px 50px 0' } },
                external__react__default.a.createElement(
                  'div',
                  { style: { paddingBottom: 15 } },
                  serverState == 'offline' ? external__react__default.a.createElement(alert__default.a, { message: '\u8FDE\u63A5\u670D\u52A1\u5668\u5931\u8D25', type: 'warning', closable: true }) : serverState == 'invalid accessCode' ? external__react__default.a.createElement(alert__default.a, { message: '\u6CE8\u518C\u7801\u65E0\u6548', type: 'warning', closable: true }) : null
                ),
                external__react__default.a.createElement(
                  'div',
                  {
                    style: { background: '#fff', padding: 24, minHeight: 380 }
                  },
                  external__react__default.a.createElement(table__default.a, { dataSource: devices, columns: columns })
                )
              ),
              external__react__default.a.createElement(
                Footer,
                { style: { textAlign: 'center' } },
                '\xA9 ' + {"author":"深圳市规格电子有限公司(https://guige.io)","buildDate":"2018-9-1 23:07:55","buildYear":2018}.buildYear + ' ',
                external__react__default.a.createElement(
                  'a',
                  { href: 'https://guige.io', target: '_blank' },
                  '\u516C\u53F8\u540D\u79F0\u53CA\u7F51\u7AD9\u94FE\u63A5'
                ),
                external__react__default.a.createElement(
                  'span',
                  { style: { color: '#ccc' } },
                  '\uFF08Build Date: ' + {"author":"深圳市规格电子有限公司(https://guige.io)","buildDate":"2018-9-1 23:07:55","buildYear":2018}.buildDate + '\uFF09'
                )
              )
            ),
            external__react__default.a.createElement(
              modal__default.a,
              {
                title: '\u53C2\u6570\u8BBE\u7F6E',
                visible: visible,
                okText: '\u4FDD\u5B58',
                cancelText: '\u53D6\u6D88',
                footer: null,
                onCancel: this.handleCancel
              },
              external__react__default.a.createElement(AccessCodeSettings, {
                accessCode: accessCode,
                handleSubmit: this.handleSubmit
              }),
              external__react__default.a.createElement(
                collapse__default.a,
                { bordered: false },
                external__react__default.a.createElement(
                  Panel,
                  {
                    header: '\u5176\u5B83\u8BBE\u7F6E',
                    key: '1',
                    style: { border: 0, color: 'red' },
                    forceRender: true
                  },
                  external__react__default.a.createElement(ServerSettings, {
                    server: server,
                    handleSubmit: this.handleSubmit
                  }),
                  external__react__default.a.createElement(GatewaySettings, {
                    dhcp: dhcp,
                    ip: ip,
                    mask: mask,
                    gateway: gateway,
                    dns: dns,
                    handleSubmit: this.handleSubmit
                  })
                )
              )
            )
          )
        )
      );
    }
  }]);

  return Home;
}(external__react_["Component"]);

/* harmony default export */ var pages = __webpack_exports__["default"] = (pages_Home);

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/locale-provider");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/modal");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/table");

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/alert");

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/col");

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/row");

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/message");

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/collapse");

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/layout");

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = require("react-shapes");

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/radio");

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = require("antd/lib/locale-provider/zh_CN");

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = require("moment/locale/zh-cn");

/***/ }),
/* 23 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);