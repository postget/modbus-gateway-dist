webpackHotUpdate(4,{

/***/ "./pages/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_antd_lib_locale_provider__ = __webpack_require__("./node_modules/antd/lib/locale-provider/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_antd_lib_locale_provider___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_antd_lib_locale_provider__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_antd_lib_modal__ = __webpack_require__("./node_modules/antd/lib/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_antd_lib_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_antd_lib_modal__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd_lib_table__ = __webpack_require__("./node_modules/antd/lib/table/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_antd_lib_table___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_antd_lib_table__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_antd_lib_alert__ = __webpack_require__("./node_modules/antd/lib/alert/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_antd_lib_alert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_antd_lib_alert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd_lib_col__ = __webpack_require__("./node_modules/antd/lib/col/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_antd_lib_col___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_antd_lib_col__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_antd_lib_button__ = __webpack_require__("./node_modules/antd/lib/button/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_antd_lib_button___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_antd_lib_button__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_antd_lib_row__ = __webpack_require__("./node_modules/antd/lib/row/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_antd_lib_row___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_antd_lib_row__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_antd_lib_message__ = __webpack_require__("./node_modules/antd/lib/message/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_antd_lib_message___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_antd_lib_message__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_antd_lib_collapse__ = __webpack_require__("./node_modules/antd/lib/collapse/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_antd_lib_collapse___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_antd_lib_collapse__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_antd_lib_layout__ = __webpack_require__("./node_modules/antd/lib/layout/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_antd_lib_layout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_antd_lib_layout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_react_shapes__ = __webpack_require__("./node_modules/react-shapes/lib/Shapes.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_react_shapes___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_react_shapes__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_AccessCodeSettings__ = __webpack_require__("./components/AccessCodeSettings.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_ServerSettings__ = __webpack_require__("./components/ServerSettings.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_GatewaySettings__ = __webpack_require__("./components/GatewaySettings.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_axios__ = __webpack_require__("./node_modules/axios/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_antd_lib_locale_provider_zh_CN__ = __webpack_require__("./node_modules/antd/lib/locale-provider/zh_CN.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_antd_lib_locale_provider_zh_CN___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_antd_lib_locale_provider_zh_CN__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_moment_locale_zh_cn__ = __webpack_require__("./node_modules/moment/locale/zh-cn.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_moment_locale_zh_cn___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_moment_locale_zh_cn__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ant_theme_vars_less__ = __webpack_require__("./ant-theme-vars.less");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ant_theme_vars_less___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_18__ant_theme_vars_less__);










var _jsxFileName = '/Users/postget/Documents/Design/node/modbus-gateway/web/pages/index.js';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/patch.js").enterModule;

  enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Header = __WEBPACK_IMPORTED_MODULE_9_antd_lib_layout___default.a.Header,
    Content = __WEBPACK_IMPORTED_MODULE_9_antd_lib_layout___default.a.Content,
    Footer = __WEBPACK_IMPORTED_MODULE_9_antd_lib_layout___default.a.Footer;
var Panel = __WEBPACK_IMPORTED_MODULE_8_antd_lib_collapse___default.a.Panel;












var Home = function (_Component) {
  _inherits(Home, _Component);

  function Home() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Home);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Home.__proto__ || Object.getPrototypeOf(Home)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      visible: false,
      accessCode: '',
      server: '',

      serverState: 'online',
      localConfig: {},
      serverConfig: {}
    }, _this.showModal = function () {
      _this.setState({
        visible: true
      });
    }, _this.handleCancel = function (e) {
      _this.setState({
        visible: false
      });
    }, _this.handleSubmit = function (values) {
      _this.configServer.post('/localConfig', values).then(function (data) {
        __WEBPACK_IMPORTED_MODULE_7_antd_lib_message___default.a.success('设置成功');
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Home, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      this.configServer = __WEBPACK_IMPORTED_MODULE_15_axios___default.a.create({
        baseURL: '//' + window.location.hostname + ':9902/'
      });

      this.configServer.get('/config').then(function (response) {
        _this2.setState(response.data);
      }).catch(function (err) {
        __WEBPACK_IMPORTED_MODULE_7_antd_lib_message___default.a.error('获取配置失败');
      });

      this.timer = setInterval(function () {
        _this2.configServer.get('/config').then(function (response) {
          _this2.setState(response.data);
        });
      }, 5000);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      clearInterval(this.timer);
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          visible = _state.visible,
          serverState = _state.serverState,
          localConfig = _state.localConfig,
          serverConfig = _state.serverConfig;
      var accessCode = localConfig.accessCode,
          server = localConfig.server,
          dhcp = localConfig.dhcp,
          ip = localConfig.ip,
          mask = localConfig.mask,
          gateway = localConfig.gateway,
          dns = localConfig.dns;
      var code = serverConfig.code,
          note = serverConfig.note,
          state = serverConfig.state,
          _serverConfig$modbusT = serverConfig.modbusTcpDevices,
          modbusTcpDevices = _serverConfig$modbusT === undefined ? [] : _serverConfig$modbusT,
          _serverConfig$modbusR = serverConfig.modbusRtuDevices,
          modbusRtuDevices = _serverConfig$modbusR === undefined ? [] : _serverConfig$modbusR;


      var devices = [];

      if (serverState == 'online') {
        modbusTcpDevices.forEach(function (device, i) {
          devices.push({
            key: 'tcp-' + i,
            code: device.code,
            note: device.note,
            typeName: device.typeName,
            address: device.ip + ':' + device.port,
            state: device.state,
            protocol: 'Modbus TCP'
          });
        });

        modbusRtuDevices.forEach(function (device, i) {
          devices.push({
            key: 'rtu-' + i,
            code: device.code,
            note: device.note,
            typeName: device.typeName,
            address: '' + device.slaveId,
            state: device.state,
            protocol: 'Modbus RTU'
          });
        });
      }

      var columns = [{
        title: '设备编号',
        dataIndex: 'code',
        key: 'code'
      }, {
        title: '备注',
        dataIndex: 'note',
        key: 'note'
      }, {
        title: '设备类型',
        dataIndex: 'typeName',
        key: 'typeName'
      }, {
        title: '通信协议',
        dataIndex: 'protocol',
        key: 'protocol'
      }, {
        title: '从机地址',
        dataIndex: 'address',
        key: 'address'
        // {
        //   title: '连接状态',
        //   dataIndex: 'state',
        //   key: 'state',
        //   render: text => (
        //     <Circle
        //       r={10}
        //       fill={{
        //         color: text == 'offline' ? '#f04134' : '#00bf60'
        //       }}
        //       stroke={{ color: 'white' }}
        //       strokeWidth={3}
        //     />
        //   )
        // }
      }];

      return __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_0_antd_lib_locale_provider___default.a,
        { locale: __WEBPACK_IMPORTED_MODULE_16_antd_lib_locale_provider_zh_CN___default.a, __source: {
            fileName: _jsxFileName,
            lineNumber: 167
          }
        },
        __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_6_antd_lib_row___default.a,
          { type: 'flex', justify: 'center', __source: {
              fileName: _jsxFileName,
              lineNumber: 168
            }
          },
          __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_4_antd_lib_col___default.a,
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 169
              }
            },
            __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_9_antd_lib_layout___default.a,
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 170
                }
              },
              __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                Header,
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 171
                  }
                },
                __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_6_antd_lib_row___default.a,
                  {
                    type: 'flex',
                    justify: 'space-between',
                    style: { minWidth: 800 },
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 172
                    }
                  },
                  __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                    __WEBPACK_IMPORTED_MODULE_6_antd_lib_row___default.a,
                    { type: 'flex', __source: {
                        fileName: _jsxFileName,
                        lineNumber: 177
                      }
                    },
                    __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                      'h2',
                      {
                        style: {
                          color: 'white',
                          padding: '0 15px',
                          background: 'rgba(255,255,255,.2)'
                        },
                        __source: {
                          fileName: _jsxFileName,
                          lineNumber: 178
                        }
                      },
                      'Modbus Gateway'
                    ),
                    serverState == 'online' && code ? __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                      'div',
                      {
                        style: {
                          marginLeft: 30,
                          color: 'white'
                        },
                        __source: {
                          fileName: _jsxFileName,
                          lineNumber: 188
                        }
                      },
                      __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                        'div',
                        {
                          style: {
                            marginTop: -10,
                            height: 23,
                            display: 'flex'
                          },
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 194
                          }
                        },
                        __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                          'span',
                          { style: { fontSize: 18 }, __source: {
                              fileName: _jsxFileName,
                              lineNumber: 201
                            }
                          },
                          code
                        )
                      ),
                      __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                        'div',
                        {
                          __source: {
                            fileName: _jsxFileName,
                            lineNumber: 215
                          }
                        },
                        __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                          'span',
                          { style: { color: '#ccc' }, __source: {
                              fileName: _jsxFileName,
                              lineNumber: 216
                            }
                          },
                          note
                        )
                      )
                    ) : null
                  ),
                  __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                    __WEBPACK_IMPORTED_MODULE_4_antd_lib_col___default.a,
                    {
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 222
                      }
                    },
                    __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_antd_lib_button___default.a, {
                      shape: 'circle',
                      icon: 'setting',
                      onClick: this.showModal,
                      __source: {
                        fileName: _jsxFileName,
                        lineNumber: 223
                      }
                    })
                  )
                )
              ),
              __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                Content,
                { style: { padding: '15px 50px 0' }, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 232
                  }
                },
                __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                  'div',
                  { style: { paddingBottom: 15 }, __source: {
                      fileName: _jsxFileName,
                      lineNumber: 233
                    }
                  },
                  serverState == 'offline' ? __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_antd_lib_alert___default.a, { message: '\u8FDE\u63A5\u670D\u52A1\u5668\u5931\u8D25', type: 'warning', closable: true, __source: {
                      fileName: _jsxFileName,
                      lineNumber: 235
                    }
                  }) : serverState == 'invalid accessCode' ? __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_antd_lib_alert___default.a, { message: '\u6CE8\u518C\u7801\u65E0\u6548', type: 'warning', closable: true, __source: {
                      fileName: _jsxFileName,
                      lineNumber: 237
                    }
                  }) : null
                ),
                __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                  'div',
                  {
                    style: { background: '#fff', padding: 24, minHeight: 380 },
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 241
                    }
                  },
                  __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_antd_lib_table___default.a, { dataSource: devices, columns: columns, __source: {
                      fileName: _jsxFileName,
                      lineNumber: 244
                    }
                  })
                )
              ),
              __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                Footer,
                { style: { textAlign: 'center' }, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 247
                  }
                },
                '\xA9 ' + {"author":"深圳市规格电子有限公司(https://guige.io)","buildDate":"2018-3-26 18:22:11","buildYear":2018}.buildYear + ' ',
                __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                  'a',
                  { href: 'https://guige.io', target: '_blank', __source: {
                      fileName: _jsxFileName,
                      lineNumber: 249
                    }
                  },
                  '\u516C\u53F8\u540D\u79F0\u53CA\u7F51\u7AD9\u94FE\u63A5'
                ),
                __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                  'span',
                  { style: { color: '#ccc' }, __source: {
                      fileName: _jsxFileName,
                      lineNumber: 252
                    }
                  },
                  '\uFF08Build Date: ' + {"author":"深圳市规格电子有限公司(https://guige.io)","buildDate":"2018-3-26 18:22:11","buildYear":2018}.buildDate + '\uFF09'
                )
              )
            ),
            __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_1_antd_lib_modal___default.a,
              {
                title: '\u53C2\u6570\u8BBE\u7F6E',
                visible: visible,
                okText: '\u4FDD\u5B58',
                cancelText: '\u53D6\u6D88',
                footer: null,
                onCancel: this.handleCancel,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 258
                }
              },
              __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_12__components_AccessCodeSettings__["a" /* default */], {
                accessCode: accessCode,
                handleSubmit: this.handleSubmit,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 266
                }
              }),
              __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_8_antd_lib_collapse___default.a,
                { bordered: false, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 271
                  }
                },
                __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(
                  Panel,
                  {
                    header: '\u5176\u5B83\u8BBE\u7F6E',
                    key: '1',
                    style: { border: 0, color: 'red' },
                    forceRender: true,
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 272
                    }
                  },
                  __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_13__components_ServerSettings__["a" /* default */], {
                    server: server,
                    handleSubmit: this.handleSubmit,
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 278
                    }
                  }),
                  __WEBPACK_IMPORTED_MODULE_11_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_14__components_GatewaySettings__["a" /* default */], {
                    dhcp: dhcp,
                    ip: ip,
                    mask: mask,
                    gateway: gateway,
                    dns: dns,
                    handleSubmit: this.handleSubmit,
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 282
                    }
                  })
                )
              )
            )
          )
        )
      );
    }
  }, {
    key: '__reactstandin__regenerateByEval',
    value: function __reactstandin__regenerateByEval(key, code) {
      this[key] = eval(code);
    }
  }]);

  return Home;
}(__WEBPACK_IMPORTED_MODULE_11_react__["Component"]);

var _default = Home;


/* harmony default export */ __webpack_exports__["default"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/patch.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/patch.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Header, 'Header', '/Users/postget/Documents/Design/node/modbus-gateway/web/pages/index.js');
  reactHotLoader.register(Content, 'Content', '/Users/postget/Documents/Design/node/modbus-gateway/web/pages/index.js');
  reactHotLoader.register(Footer, 'Footer', '/Users/postget/Documents/Design/node/modbus-gateway/web/pages/index.js');
  reactHotLoader.register(Panel, 'Panel', '/Users/postget/Documents/Design/node/modbus-gateway/web/pages/index.js');
  reactHotLoader.register(Home, 'Home', '/Users/postget/Documents/Design/node/modbus-gateway/web/pages/index.js');
  reactHotLoader.register(_default, 'default', '/Users/postget/Documents/Design/node/modbus-gateway/web/pages/index.js');
  leaveModule(module);
})();

;
    (function (Component, route) {
      if(!Component) return
      if (false) return
      module.hot.accept()
      Component.__route = route

      if (module.hot.status() === 'idle') return

      var components = next.router.components
      for (var r in components) {
        if (!components.hasOwnProperty(r)) continue

        if (components[r].Component.__route === route) {
          next.router.update(r, Component)
        }
      }
    })(typeof __webpack_exports__ !== 'undefined' ? __webpack_exports__.default : (module.exports.default || module.exports), "/")
  
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=4.85fc0a1f2b8335d29ad6.hot-update.js.map